# tachyon

algorithmic equities trading engine with Alpaca + Polygon

# NOTE: Exposed snapshot to provide a code sample for internship apps

All profitable strategies have been removed; see `src/inverse_momentum.rs` as an example strategy.

# feature overview

- Polygon.io market data websocket API
- Alpaca.markets REST + websocket APIS (partial implementation, only features needed for the strategies we run)
- fully asynchronous networking and strategy event loops (200us observed latency between data reception via Polygon websocket and trade initiation via Alpaca REST request)
- hashbrown HashMaps, jemalloc, and Rustls by default for enhanced performance
- systemd service and timer units
- timezone and holiday aware via pandas_market_calendars
- logging via stdout