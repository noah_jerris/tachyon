#![allow(unused)]

pub use rust_decimal::{Decimal, RoundingStrategy};
use serde::de::{self, Visitor};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::fmt;
use uuid::Uuid;

/// An ID used to specify an asset.
/// Uses a fixed-length string to make assumptions that boost performance.
#[derive(Debug, Clone, Copy, Eq, Hash, PartialEq)]
pub struct AssetID([u8; 8]);

impl AssetID {
    pub fn from_symbol(symbol: &str) -> AssetID {
        let bytes = symbol.as_bytes();
        let mut array = [0; 8];
        for pair in array.iter_mut().zip(bytes.iter()) {
            let (dst, src) = pair;
            *dst = *src;
        }

        AssetID(array)
    }

    pub fn to_symbol(&self) -> String {
        // find the null-terminated end
        let mut end = self.0.len();
        for i in 0..self.0.len() {
            if self.0[i] == 0 {
                end = i;
                break;
            }
        }

        // guaranteed to be safe because the id was created from a valid UTF-8 sequence
        unsafe { std::str::from_utf8_unchecked(&self.0[0..end]).into() }
    }
}

impl fmt::Display for AssetID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_symbol())
    }
}

impl<'de> Deserialize<'de> for AssetID {
    fn deserialize<D>(deserializer: D) -> std::result::Result<AssetID, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct AssetIDVisitor;

        impl<'de> Visitor<'de> for AssetIDVisitor {
            type Value = AssetID;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("string containing ticker symbol")
            }

            fn visit_str<E>(self, value: &str) -> std::result::Result<AssetID, E>
            where
                E: de::Error,
            {
                Ok(AssetID::from_symbol(value))
            }
        }

        deserializer.deserialize_identifier(AssetIDVisitor)
    }
}

impl Serialize for AssetID {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.to_symbol())
    }
}

/// A single trade.
#[derive(Clone, Debug)]
pub struct Trade {
    pub asset: AssetID,
    pub price: Decimal,
    pub size: Decimal,
    pub timestamp: i64,
}

/// A single quote.
#[derive(Clone, Debug)]
pub struct Quote {
    pub asset: AssetID,
    pub bid: Decimal,
    pub bid_size: Decimal,
    pub ask: Decimal,
    pub ask_size: Decimal,
    pub timestamp: i64,
}

/// A single unit of market data.
#[derive(Clone, Debug)]
pub enum MktData {
    Trade(Trade),
    Quote(Quote),
}

/// An object used to select a type of market data associated with a asset.
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum MktDataRequest {
    Trades(AssetID),
    Quotes(AssetID),
}

/// An ID used to specify an order.
#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub struct OrderID(Uuid);

impl fmt::Display for OrderID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0.to_simple())
    }
}

/// An order.
#[derive(Clone, Debug)]
pub struct Order {
    pub asset: AssetID,
    pub qty: Decimal,
    pub limit: Decimal,
}

impl Order {
    /// Adjust the order's limit price by delta.
    /// NOTE: positive delta -> more favorable, negative delta -> less favorable
    pub fn apply_delta(&mut self, delta: Decimal) {
        if self.qty > Decimal::from(0) {
            // buying, add delta
            self.limit += delta;
        } else if self.qty < Decimal::from(0) {
            // selling, subtract delta
            self.limit -= delta;
        } 
    }
}

/// The status of an order.
#[derive(Clone, Debug)]
pub struct OrderStatus {
    pub id: OrderID,
    pub filled_qty: Decimal,
    pub filled_price: Option<Decimal>,
    pub finished: bool,
    pub replacement: Option<OrderID>,
}
