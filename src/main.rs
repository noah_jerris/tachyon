//! tachyon automated portfolio management framework

#![recursion_limit = "256"]

mod analytics;
mod broker;
mod inverse_momentum;
mod mktdata;
mod types;

use broker::*;
use chrono::{DateTime, Utc};
use clap::Clap;
use hashbrown::HashSet;
use mktdata::*;
use serde::Deserialize;
use tokio::sync::{broadcast, mpsc};
use tokio::task::JoinHandle;
use types::*;

// Use jemalloc for superior performance over the system allocator.
#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

pub const ALPACA_KEY_ID: &str = "************";
pub const ALPACA_SECRET_KEY: &str = "***********";
pub const POLYGON_KEY: &str = "***********";

pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[tokio::main]
async fn main() -> Result<()> {
    // parse arguments
    let args = Args::parse();

    // read the configuration file
    let strategies = ron::from_str(
        &tokio::fs::read_to_string(&args.config_path)
            .await
            .expect("failed to locate configuration file"),
    )?;

    if managed_asset_conflicts(&strategies) {
        return Err("specified configuration has managed asset conflicts".into());
    }

    // start all strategies
    let tasks = launch_strategies(strategies, args.mkt_open, args.mkt_close).await?;

    // and wait for all strategies to finish
    futures::future::join_all(tasks).await;

    Ok(())
}

// Required arguments.
#[derive(Clap)]
struct Args {
    mkt_open: DateTime<Utc>,
    mkt_close: DateTime<Utc>,
    config_path: String,
}

/// An enum represending configuration options for a strategy.
#[derive(Debug, Deserialize)]
pub enum Strategy {
    InverseMomentum(inverse_momentum::Config),
}

// An endpoint to access local tachyon services.
#[derive(Debug)]
pub struct Endpoint {
    mktdata_rx: broadcast::Receiver<MktData>,
    broker_request_tx: mpsc::Sender<broker::Request>,
    order_update_rx: broadcast::Receiver<OrderStatus>,
}

// Check for conflicts in managed assets. This must be checked because
// multiple strategies cannot trade the same asset at this time.
fn managed_asset_conflicts(strategies: &Vec<Strategy>) -> bool {
    let mut managed_assets = HashSet::new();
    for strategy in strategies {
        match strategy {
            Strategy::InverseMomentum(config) => {
                for managed_asset in config.managed_assets() {
                    if !managed_assets.insert(managed_asset) {
                        // managed_asset is not unique
                        return true;
                    }
                }
            }
        }
    }
    false
}

// Get the market data required by all configured strategies.
fn required_mktdata(strategies: &Vec<Strategy>) -> HashSet<MktDataRequest> {
    // accumulate data requests in a hash set
    let mut requests = Vec::new();
    for strategy in strategies {
        match strategy {
            Strategy::InverseMomentum(config) => {
                requests.extend(config.required_mktdata());
            }
        }
    }

    use std::iter::FromIterator;
    let mktdata = HashSet::from_iter(requests.into_iter());
    mktdata
}

// Create endpoints and start tachyon services.
async fn make_endpoints(strategies: &Vec<Strategy>) -> Result<Vec<Endpoint>> {
    // channel for market data
    let (mktdata_tx, mktdata_rx) = broadcast::channel(1024);

    // channel for order requests
    let (broker_action_tx, action_rx) = mpsc::channel(1024);

    // channel for order updates
    let (update_tx, order_update_rx) = broadcast::channel(1024);

    for strategy in strategies {
        let managed_assets = match strategy {
            Strategy::InverseMomentum(config) => config.managed_assets(),
        };
        for asset in managed_assets {
            if !broker::can_borrow(asset).await? {
                // don't create new position object
            }
        }
    }

    // create endpoints for each strategy
    let mut endpoints = Vec::new();
    for _i in 0..strategies.len() {
        endpoints.push(Endpoint {
            mktdata_rx: mktdata_tx.subscribe(),
            broker_request_tx: broker_action_tx.clone(),
            order_update_rx: update_tx.subscribe(),
        });
    }

    // then use the remaining items to make a final endpoint
    endpoints.push(Endpoint {
        mktdata_rx: mktdata_rx,
        broker_request_tx: broker_action_tx,
        order_update_rx: order_update_rx,
    });

    // collect required market data
    let required_mktdata = required_mktdata(strategies);

    // start local tachyon services
    std::thread::spawn(|| {
        broadcast_mktdata(mktdata_tx, required_mktdata);
    });
    std::thread::spawn(|| {
        broadcast_order_updates(update_tx);
    });
    tokio::spawn(async {
        multiplex_broker(action_rx).await;
    });

    Ok(endpoints)
}

// Launch all configured strategies.
async fn launch_strategies(
    strategies: Vec<Strategy>,
    mkt_open: DateTime<Utc>,
    mkt_close: DateTime<Utc>,
) -> Result<Vec<JoinHandle<()>>> {
    let mut endpoints = make_endpoints(&strategies).await?;

    let mut tasks = vec![];
    for strategy in strategies {
        // Note: expect because make_endpoints should have made self.strategies.len() + 1
        // endpoints
        match strategy {
            Strategy::InverseMomentum(config) => {
                let endpoint = endpoints.pop().expect("insufficient endpoints");
                tasks.push(tokio::spawn(async move {
                    inverse_momentum::run(config, endpoint, mkt_open, mkt_close).await;
                }));
            }
        }
    }

    Ok(tasks)
}
