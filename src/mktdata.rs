use crate::types::*;
use crate::POLYGON_KEY;
use hashbrown::HashSet;
use serde::{Deserialize, Serialize};
use std::net::TcpStream;
use tokio::sync::broadcast::Sender;
use tungstenite::Message;
use url::Url;

/// Broadcast the specified set of market data from Polygon.io over a tokio broadcast
/// channel. Note that broadcast_mktdata is *NOT* async - it is highly latency-sensitive,
/// so it shouldn't run in an async environment (preliminary benchmarks showed
/// tokio_tungstenite was 200-300% slower than vanilla tungstenite).
pub fn broadcast_mktdata(mut channel_tx: Sender<MktData>, required_data: HashSet<MktDataRequest>) {
    // NOTE: unwrap is permitted for initialization code that shouldn't fail

    // handle as much rustls configuration outside of the networking loop as possible
    let mut config = rustls::ClientConfig::new();
    config
        .root_store
        .add_server_trust_anchors(&webpki_roots::TLS_SERVER_ROOTS);
    let arc = std::sync::Arc::new(config);
    let dns_name = webpki::DNSNameRef::try_from_ascii_str("polygon.io").unwrap();
    let ws_url = Url::parse("wss://socket.polygon.io/stocks").unwrap();

    // also serialize outgoing messages now
    let auth_msg = serde_json::to_string(&Request::authentication(POLYGON_KEY)).unwrap();
    let subs_msg = serde_json::to_string(&Request::subscription(&required_data)).unwrap();

    // outer loop to continuously retry connect-broadcast sequence
    // NOTE: don't use methods that panic anywhere in the loop
    loop {
        // inner loop to handle connection sequence failures
        loop {
            // set up a TLS stream with Polygon
            let mut tcpstream;
            if let Ok(s) = TcpStream::connect("socket.polygon.io:443") {
                tcpstream = s;
            } else {
                break;
            }
            let mut client = rustls::ClientSession::new(&arc, dns_name);
            let stream = rustls::Stream::new(&mut client, &mut tcpstream);

            // open a WebSocket connection over the stream and ignore Polygon's
            // connection message
            let mut socket;
            if let Ok((s, _)) = tungstenite::client::client(&ws_url, stream) {
                socket = s;
            } else {
                break;
            }
            drop(socket.read_message());

            // authenticate
            if let Err(_) = socket.write_message(Message::Text(auth_msg.clone())) {
                break;
            }
            if let Ok(Message::Text(resp)) = socket.read_message() {
                if resp != AUTH_SUCCESS {
                    break;
                }
            } else {
                break;
            }

            // request market data and ignore response
            if let Err(_) = socket.write_message(Message::Text(subs_msg.clone())) {
                break;
            }
            drop(socket.read_message());

            // finally one more loop to read the WebSocket to failure
            loop {
                match socket.read_message() {
                    Ok(Message::Text(json)) => {
                        match serde_json::from_str::<Vec<DataEvent>>(&json) {
                            Ok(messages) => handle_messages(messages, &mut channel_tx),
                            Err(_) => (),
                            // do some kind of logging on error
                            //eprintln!("failed to deserialize Polygon.io message: {}", &json),
                        }
                    }
                    Err(_) | Ok(Message::Close(_)) => {
                        break;
                    }
                    _ => (),
                }
            }
            // at this point, the socket closed or an error occured
            // loop to reconnect
            println!("{}: Polygon disconnection", chrono::Utc::now());
        }
        // at this point, the connection sequence failed
        // loop to retry the connection sequence
        println!("{}: Polygon connection sequence failed", chrono::Utc::now());
    }
}

// Broadcast a vector of received messages.
fn handle_messages(messages: Vec<DataEvent>, channel_tx: &mut Sender<MktData>) {
    for ref message in messages {
        let data = match message {
            DataEvent::Trade(t) => MktData::Trade(t.into()),
            DataEvent::Quote(q) => MktData::Quote(q.into()),
        };

        // send the message over the tokio broadcast channel
        // NOTE: expect is permitted because data should be broadcast
        // only as long as there are entities consuming it
        channel_tx.send(data).expect("mktdata channel dropped");
    }
}

#[derive(Debug, Serialize)]
struct Request {
    action: String,
    params: String,
}

impl Request {
    fn authentication(key: &str) -> Request {
        Request {
            action: "auth".into(),
            params: key.into(),
        }
    }

    fn subscription(selectors: &HashSet<MktDataRequest>) -> Request {
        let mut keys = String::new();
        for selector in selectors.iter() {
            let key = match selector {
                MktDataRequest::Trades(id) => format!("T.{},", id.to_symbol()),
                MktDataRequest::Quotes(id) => format!("Q.{},", id.to_symbol()),
            };
            keys.push_str(&key);
        }

        Request {
            action: "subscribe".into(),
            params: keys,
        }
    }
}

const AUTH_SUCCESS: &str =
    "[{\"ev\":\"status\",\"status\":\"auth_success\",\"message\":\"authenticated\"}]";

#[derive(Debug, Deserialize)]
#[serde(tag = "ev")]
enum DataEvent {
    #[serde(rename = "T")]
    Trade(TradeMsg),
    #[serde(rename = "Q")]
    Quote(QuoteMsg),
}

#[derive(Debug, Deserialize)]
struct TradeMsg {
    sym: AssetID, // symbol (deserialized as AssetID)
    p: Decimal,   // price
    s: Decimal,   // trade size
    t: i64,       // timestamp (ms)
}

impl Into<Trade> for &TradeMsg {
    fn into(self) -> Trade {
        Trade {
            asset: self.sym,
            price: self.p,
            size: self.s,
            timestamp: self.t,
        }
    }
}

#[derive(Debug, Deserialize)]
struct QuoteMsg {
    sym: AssetID,  // symbol (deserialized as AssetID)
    bx: u64,       // bix exchange id
    bp: Decimal,   // bid price
    bs: Decimal,   // bid size
    ax: u64,       // ask exchange id
    ap: Decimal,   // ask price
    r#as: Decimal, // ask size
    t: i64,        // timestamp (ms)
}

impl Into<Quote> for &QuoteMsg {
    fn into(self) -> Quote {
        Quote {
            asset: self.sym,
            bid: self.bp,
            bid_size: self.bs,
            ask: self.ap,
            ask_size: self.r#as,
            timestamp: self.t,
        }
    }
}
