use crate::types::*;
use crate::{ALPACA_KEY_ID, ALPACA_SECRET_KEY};
use reqwest::Client;
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::net::TcpStream;
use tokio::sync::{broadcast, mpsc, oneshot};
use tungstenite::Message;
use url::Url;

// TODO: use UUID order ids instead of symbols
//use uuid::Uuid;

/// Errors that may occur when interacting with the broker.
#[derive(Debug, PartialEq)]
pub enum Error {
    /// Failure of internal components, generally fatal.
    Internal,

    /// Inavalid Alpaca API response, not necessairily fatal.
    InvalidResponse,

    /// The quantity of a requested order is 0.
    NullOrder,

    /// Rejection of a request (i.e. to cancel an order).
    Rejection,
}

type Result<T> = std::result::Result<T, Error>;

/*
/// A change to an order for order replacement.
#[derive(Clone, Debug)]
pub struct OrderChange {
    new_qty: Option<Decimal>,
    new_limit: Option<Decimal>,
}
*/

#[derive(Debug)]
pub enum Request {
    Submit(Order, oneshot::Sender<Result<OrderID>>),
    Replace(OrderID, Order, oneshot::Sender<Result<OrderID>>),
    Cancel(OrderID, oneshot::Sender<Result<()>>),
}

/// A response to a broker request.
pub type SubmitResponse = oneshot::Receiver<Result<OrderID>>;
pub type CancelResponse = oneshot::Receiver<Result<()>>;

/// Submit an order.
/// NOTE: submit() returns None if it could not send a request over the broker channel,
/// this may be considered fatal.
pub async fn submit(ch: &mut mpsc::Sender<Request>, order: Order) -> Option<SubmitResponse> {
    let (tx, rx) = oneshot::channel();
    match ch.send(Request::Submit(order, tx)).await {
        Ok(_) => Some(rx),
        Err(_) => None,
    }
}

/// Replace an order.
/// NOTE: submit() returns None if it could not send a request over the broker channel,
/// this may be considered fatal.
pub async fn replace(ch: &mut mpsc::Sender<Request>, id: OrderID, order: Order) -> Option<SubmitResponse> {
    let (tx, rx) = oneshot::channel();
    match ch.send(Request::Replace(id, order, tx)).await {
        Ok(_) => Some(rx),
        Err(_) => None,
    }
}

/// Cancel an order.
/// NOTE: cancel() returns None if it could not send a request over the broker channel,
/// this may be considered fatal.
pub async fn cancel(ch: &mut mpsc::Sender<Request>, id: OrderID) -> Option<CancelResponse> {
    let (tx, rx) = oneshot::channel();
    match ch.send(Request::Cancel(id, tx)).await {
        Ok(_) => Some(rx),
        Err(_) => None,
    }
}

/// Check if the given asset can be borrowed. In particular, we are concerned about
/// "easy to borrow" status.
pub async fn can_borrow(asset: AssetID) -> std::result::Result<bool, Box<dyn std::error::Error>> {
    // the object returned from the API contains many more fields than these, but
    // they are the only ones relevant right now
    #[derive(Deserialize)]
    struct AssetProperties {
        tradable: bool,
        marginable: bool,
        shortable: bool,
        easy_to_borrow: bool,
    }

    // make a request to the API and deserialize the response
    let uri = format!("https://api.alpaca.markets/v2/assets/{}", asset.to_symbol());
    let a = Client::new()
        .get(&uri)
        .header("APCA-API-KEY-ID", ALPACA_KEY_ID)
        .header("APCA-API-SECRET-KEY", ALPACA_SECRET_KEY)
        .send()
        .await?
        .json::<AssetProperties>()
        .await?;

    // 'easy_to_borrow' is most important, but we check other attributes just in case
    // Alpaca's API is inconsistent with the documentation
    if a.tradable && a.marginable && a.shortable && a.easy_to_borrow {
        Ok(true)
    } else {
        Ok(false)
    }
}

/// Multiplex broker requests from a tokio mpsc channel to Alpaca.markets.
pub async fn multiplex_broker(mut request_rx: mpsc::Receiver<Request>) {
    // single https client allows reuse of TLS connections
    let client = Client::new();
    loop {
        // handle requests
        if let Some(action) = request_rx.recv().await {
            match action {
                Request::Submit(order, rsp_tx) => match prepare_order(order) {
                    Ok(form) => {
                        let request = client
                            .post("https://api.alpaca.markets/v2/orders")
                            .header("APCA-API-KEY-ID", ALPACA_KEY_ID)
                            .header("APCA-API-SECRET-KEY", ALPACA_SECRET_KEY)
                            .body(form);
                        tokio::spawn(async move {
                            await_submit_response(request, rsp_tx).await;
                        });
                    },
                    Err(e) => {
                        drop(rsp_tx.send(Err(e)));
                    },
                },
                Request::Replace(id, order, rsp_tx) => {
                    match prepare_replace_order(order) {
                        Ok(form) => {
                            let uri = format!("https://api.alpaca.markets/v2/orders/{}", id);
                            let request = client
                                .patch(&uri)
                                .header("APCA-API-KEY-ID", ALPACA_KEY_ID)
                                .header("APCA-API-SECRET-KEY", ALPACA_SECRET_KEY)
                                .body(form);
                            tokio::spawn(async move {
                                await_submit_response(request, rsp_tx).await;
                            });
                        },
                        Err(e) => {
                            drop(rsp_tx.send(Err(e)));
                        },
                    }
                }
                Request::Cancel(id, rsp_tx) => {
                    let uri = format!("https://api.alpaca.markets/v2/orders/{}", id);
                    let request = client
                        .delete(&uri)
                        .header("APCA-API-KEY-ID", ALPACA_KEY_ID)
                        .header("APCA-API-SECRET-KEY", ALPACA_SECRET_KEY);
                    tokio::spawn(async move {
                        await_cancel_response(request, rsp_tx).await;
                    });
                }
            }
        } else {
            panic!("all senders on the order channel have been dropped");
        }
    }
}

/// Broadcast order updates from Alpaca.markets over a tokio broadcast channel.
/// Note that broadcast_order_updates is *NOT* async - it is highly latency-sensitive,
/// so it shouldn't run in an async environment (preliminary benchmarks showed
/// tokio_tungstenite was 200-300% slower than vanilla tungstenite).
pub fn broadcast_order_updates(update_tx: broadcast::Sender<OrderStatus>) {
    // NOTE: unwrap is permitted for initialization code that shouldn't fail

    // handle as much rustls configuration outside of the networking loop as possible
    let mut config = rustls::ClientConfig::new();
    config
        .root_store
        .add_server_trust_anchors(&webpki_roots::TLS_SERVER_ROOTS);
    let arc = std::sync::Arc::new(config);
    let dns_name = webpki::DNSNameRef::try_from_ascii_str("api.alpaca.markets").unwrap();
    let ws_url = Url::parse("wss://api.alpaca.markets/stream").unwrap();

    // also serialize outgoing messages now
    let auth_msg = serde_json::to_string(&StreamRequest::authentication(
        ALPACA_KEY_ID,
        ALPACA_SECRET_KEY,
    )).unwrap();
    let subs_msg = serde_json::to_string(&StreamRequest::subscription()).unwrap();

    // outer loop to continuously retry connect-broadcast sequence
    // NOTE: don't use methods that panic anywhere in the loop
    loop {
        // inner loop to handle connection sequence failures
        loop {
            // set up a TLS stream with Alpaca
            let mut tcpstream;
            if let Ok(s) = TcpStream::connect("api.alpaca.markets:443") {
                tcpstream = s;
            } else {
                break;
            }
            let mut client = rustls::ClientSession::new(&arc, dns_name);
            let stream = rustls::Stream::new(&mut client, &mut tcpstream);

            // open a WebSocket connection over the stream
            let mut socket;
            if let Ok((s, _)) = tungstenite::client::client(&ws_url, stream) {
                socket = s;
            } else {
                break;
            }

            // authenticate
            if let Err(_) = socket.write_message(Message::Text(auth_msg.clone())) {
                break;
            }
            if let Ok(Message::Binary(resp)) = socket.read_message() {
                // NOTE: unwrap permitted because if the format of messages changed,
                // the rest of this function will fail too
                let auth_response: AlpacaWSMsg = serde_json::from_slice(&resp).unwrap();
                if !auth_response.is_authentication() {
                    break;
                }
            } else {
                break;
            }

            // subscribe to order updates and ignore response
            if let Err(_) = socket.write_message(Message::Text(subs_msg.clone())) {
                break;
            }
            drop(socket.read_message());

            // finally one more loop to read the WebSocket to failure
            loop {
                match socket.read_message() {
                    Ok(Message::Binary(json)) => {
                        match serde_json::from_slice::<AlpacaWSMsg>(&json) {
                            Ok(AlpacaWSMsg::OrderStatus(status)) => {
                                drop(update_tx.send(status.into()))
                            }
                            Err(e) => {
                                // TODO: log
                                eprintln!(
                                    "failed to deserialize Alpaca.markets message: {:?}",
                                    &json
                                );
                                eprintln!("serde_json error: {}", e);
                            }
                            _ => (),
                        }
                    }
                    Err(_) | Ok(Message::Close(_)) => {
                        break;
                    }
                    _ => (),
                }
            }
            // at this point, the socket closed or an error occured
            // loop to reconnect
            println!("{}: Alpaca disconnection", chrono::Utc::now());
        }
        // at this point, the connection sequence failed
        // loop to retry the connection sequence
        println!("{}: Alpaca connection sequence failed", chrono::Utc::now());
    }
}

// Prepare an order to get a serialized order and OrderID.
fn prepare_order(order: Order) -> Result<String> {
    // determine the side of the order
    let mut side = "";
    if order.qty == Decimal::from(0) {
        return Err(Error::NullOrder);
    } else if order.qty > Decimal::from(0) {
        side = "buy";
    } else if order.qty < Decimal::from(0) {
        side = "sell";
    }

    // convert the order to JSON according to Alpaca specs
    // NOTE: Since we only support limit orders, extended hours trading
    // is automatically available.
    let alpaca_order = json!({
        "symbol": order.asset.to_symbol(),
        "qty": format!("{}", order.qty.abs()),
        "side": side,
        "type": "limit",
        "limit_price": format!("{}", order.limit),
        "time_in_force": "day",
        "extended_hours": true,
    });

    // serialize the JSON
    Ok(serde_json::to_string(&alpaca_order).or(Err(Error::Internal))?)
}

// Prepare an order to replace another order.
fn prepare_replace_order(order: Order) -> Result<String> {
    // replacement orders only have two truly relevant fields, qty and limit
    let order_patch = json!({
        "qty": format!("{}", order.qty.abs()),
        "limit_price": format!("{}", order.limit),
    });

    // serialize the JSON
    Ok(serde_json::to_string(&order_patch).or(Err(Error::Internal))?)
}

/// Submit an order / replacement order and wait for a response.
async fn await_submit_response(
    request: reqwest::RequestBuilder,
    r_tx: oneshot::Sender<Result<OrderID>>,
) {
    // send the order submit request and wait for a response
    match request.send().await {
        Ok(resp) => {
            // got a response, check status
            if resp.status().is_success() {
                // order was placed, try to get id from returned order object
                match resp.json::<ApiOrderObject>().await {
                    Ok(order) => drop(r_tx.send(Ok(order.id))),
                    Err(_) => drop(r_tx.send(Err(Error::InvalidResponse))),
                }
            } else {
                drop(r_tx.send(Err(Error::Rejection)))
            }
        },
        Err(_) => drop(r_tx.send(Err(Error::Internal))),
    }
}

/// Request an order cancellation and wait for the response.
async fn await_cancel_response(
    request: reqwest::RequestBuilder,
    r_tx: oneshot::Sender<Result<()>>,
) {
    // send the order cancel request and wait for a response
    match request.send().await {
        Ok(resp) => {
            // got a response, check status
            if resp.status().is_success() {
                // order was cancelled
                drop(r_tx.send(Ok(())))
            } else {
                drop(r_tx.send(Err(Error::Rejection)))
            }
        },
        Err(_) => drop(r_tx.send(Err(Error::Internal))),
    }
}

#[derive(Debug, Serialize)]
#[serde(tag = "action", content = "data")]
enum StreamRequest {
    #[serde(rename = "authenticate")]
    Authenticate { key_id: String, secret_key: String },
    #[serde(rename = "listen")]
    Subscribe { streams: Vec<String> },
}

impl StreamRequest {
    // Form an authentication request with an api key.
    fn authentication(key_id: &str, secret_key: &str) -> StreamRequest {
        StreamRequest::Authenticate {
            key_id: key_id.into(),
            secret_key: secret_key.into(),
        }
    }

    // Form a subscription request to trade updates.
    fn subscription() -> StreamRequest {
        StreamRequest::Subscribe {
            streams: vec!["trade_updates".into()],
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(tag = "stream", content = "data")]
enum AlpacaWSMsg {
    #[serde(rename = "account_updates")]
    AccountUpdate,
    #[serde(rename = "authorization")]
    Authentication { status: String },
    #[serde(rename = "listening")]
    Subscription,
    #[serde(rename = "trade_updates")]
    OrderStatus(OrderStatusMsg),
}

impl AlpacaWSMsg {
    fn is_authentication(&self) -> bool {
        if let AlpacaWSMsg::Authentication { status } = self {
            if status == "authorized" {
                return true;
            } else {
                return false;
            }
        }
        false
    }
}

#[derive(Debug, Deserialize)]
struct ApiOrderObject {
    id: OrderID,
    filled_qty: Decimal,
    filled_avg_price: Option<Decimal>,
    side: String,
    replaced_by: Option<OrderID>,
}

#[derive(Debug, Deserialize)]
struct OrderStatusMsg {
    event: String,
    order: ApiOrderObject,
}

impl Into<OrderStatus> for OrderStatusMsg {
    fn into(self) -> OrderStatus {
        let fill_qty = match self.order.side.as_str() {
            "buy" => self.order.filled_qty,
            "sell" => -self.order.filled_qty,
            _ => panic!("Alpaca returned order object with side neither buy or sell"),
        };

        let finished = match self.event.as_ref() {
            // the order is finished on these events
            "canceled" | "expired" | "rejected" | "fill" | "replaced" => true,
            _ => false,
        };

        OrderStatus {
            id: self.order.id,
            filled_qty: fill_qty,
            filled_price: self.order.filled_avg_price,
            finished: finished,
            replacement: self.order.replaced_by,
        }
    }
}
