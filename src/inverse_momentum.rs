use crate::analytics::EMA;
use crate::broker;
use crate::types::*;
use crate::Endpoint;
use chrono::{DateTime, Utc};
use futures::{pin_mut, select, FutureExt};
use serde::Deserialize;
use tokio::sync::{mpsc, oneshot};
use tokio::time::{delay_for, Duration};

/// Configuration options for the inverse momentum strategy.
#[derive(Debug, Deserialize)]
pub struct Config {
    asset: AssetID,
    shares: Decimal,
    min: Decimal,
    max: Decimal,
    short_span_secs: Decimal,
    long_span_secs: Decimal,
    entry_delta: Decimal,
    exit_delta: Decimal,
    exit_miss_delta: Decimal,
    dynamic: bool,
}

impl Config {
    /// Check if the configuration is valid.
    fn is_valid(&self) -> bool {
        // long span must be strictly greater than short span
        if self.long_span_secs <= self.short_span_secs {
            return false;
        }

        true
    }

    /// Obtain managed assets given a configuration.
    pub fn managed_assets(&self) -> Vec<AssetID> {
        vec![self.asset]
    }

    /// Obtain required market given an configuration.
    pub fn required_mktdata(&self) -> Vec<MktDataRequest> {
        vec![
            MktDataRequest::Trades(self.asset),
            MktDataRequest::Quotes(self.asset),
        ]
    }
}

#[derive(Clone, Copy)]
struct OrderState {
    qty: Decimal,
    #[allow(unused)]
    limit: Decimal,
    filled_qty: Decimal,
    filled_price: Option<Decimal>,
    timestamp: i64,
}

enum PendingRequest {
    Submit {
        rsp: broker::SubmitResponse,
        qty: Decimal,
        lmt: Decimal,
    },
    Replace {
        rsp: broker::SubmitResponse,
        qty: Decimal,
        lmt: Decimal,
    },
    Cancel(broker::CancelResponse),
    None,
}

// Order executor.
struct Executor {
    asset: AssetID,

    bid: Option<Decimal>,
    ask: Option<Decimal>,

    target_shares: Decimal,
    last_shares: Decimal,

    target_price: Option<Decimal>,    
    
    exiting: bool,
    exit_limit_missed: bool,
    entry_delta: Decimal,
    exit_delta: Decimal,
    exit_miss_delta: Decimal,

    active_order: Option<(OrderID, OrderState)>,
    replace_order: Option<(OrderID, OrderState)>,

    pending_request: PendingRequest,
    failed_requests: u8,

    broker_ch: mpsc::Sender<broker::Request>,
}

impl Executor {
    // Resolve the executor to all available data.
    async fn resolve(&mut self, now_ts: i64) {
        // try to yield responses, return if a request is still pending
        self._try_yield(now_ts);
        match self.pending_request {
            PendingRequest::None => (),
            _ => return,
        }
        
        // now certain that there are no requests pending
        self._check_qty().await;
        self._check_stale(now_ts).await;
    }

    // Update the executor with a new bid and ask.
    fn update_ba(&mut self, bid: Decimal, ask: Decimal) {
        self.bid = Some(bid);
        self.ask = Some(ask);
    }

    // Update the executor with order status.
    fn update_order_status(&mut self, status: OrderStatus) {
        // ignore order updates if not waiting on an order
        if let Some((id, os)) = &mut self.active_order {
            // only care about status of pending order
            if status.id == *id {
                os.filled_qty = status.filled_qty;
                os.filled_price = status.filled_price;

                // effective position is last_shares plus the filled quantity 
                let effective_position = self.last_shares + os.qty - (os.qty - status.filled_qty);

                // if the order is finished, update last_shares and remove the order
                if status.finished {
                    self.active_order = None;
                    self.last_shares = effective_position;
                }
            }
        }

        // check replacement
        if let Some((id, _)) = &self.replace_order {
            if let Some(replacement_id) = status.replacement {
                if *id == replacement_id {
                    self.active_order = self.replace_order;
                    self.replace_order = None;
                }
            }
        }
    }

    // Check if the active order quantity will result in the target position.
    async fn _check_qty(&mut self) {
        // determine wanted quantity for the active order
        let wanted_qty;
        if self.target_shares > Decimal::from(0) && self.last_shares < Decimal::from(0) {
            // want long position but currently short, must first get to 0 shares
            wanted_qty = -self.last_shares;
            self.exiting = true;
        } else if self.target_shares < Decimal::from(0) && self.last_shares > Decimal::from(0) {
            // want short position but currently long, must first get to 0 shares
            wanted_qty = -self.last_shares;
            self.exiting = true;
        } else {
            // already on correct side of 0, go straight to target
            wanted_qty = self.target_shares - self.last_shares;
            self.exiting = false;
            self.exit_limit_missed = false;
        }

        // require order quantity == wanted quantity
        if let Some((id, os)) = &self.active_order {
            // active order, if order qty != wanted_qty...
            if os.qty != wanted_qty {
                // then we must cancel the order before we can place a new one
                if let PendingRequest::None = &self.pending_request {
                    let rx = broker::cancel(&mut self.broker_ch, *id)
                        .await
                        .expect("broker request channel closed");
                    self.pending_request = PendingRequest::Cancel(rx);
                }
            }

            // TODO: maybe replace instead of cancel for orders on same side
        } else {
            // no active order, if wanted_qty != 0...
            if wanted_qty != Decimal::from(0) {
                // then submit a new order

                // calculate delta around target price
                let delta = if self.exiting && !self.exit_limit_missed {
                    self.exit_delta
                } else if self.exiting && self.exit_limit_missed {
                    self.exit_miss_delta
                } else {
                    self.entry_delta
                };

                // create order and adjust with delta
                let mut order = Order {
                    asset: self.asset,
                    qty: wanted_qty,
                    limit: self.target_price.expect("no target price"),
                };
                order.apply_delta(delta);
                let lmt = order.limit;

                // submit order and record the request
                let rx = broker::submit(&mut self.broker_ch, order)
                    .await
                    .expect("broker request channel closed");
                self.pending_request = PendingRequest::Submit {
                    rsp: rx,
                    qty: wanted_qty,
                    lmt: lmt,
                };
            }
        }
    }

    // Check if the active order is stale.
    async fn _check_stale(&mut self, now_ts: i64) {
        // entry orders cannot be stale
        if !self.exiting {
            return
        }

        // an order must exist in order to be stale
        if let Some((id, os)) = &self.active_order {
            // if order is more than 2500ms old and exit limit was not missed
            if now_ts - os.timestamp > 2500 && !self.exit_limit_missed {
                if let PendingRequest::None = &self.pending_request {
                    self.exit_limit_missed = true;
                    let mut order = Order {
                        asset: self.asset,
                        qty: os.qty,
                        limit: self.target_price.expect("no target price"),
                    };
                    order.apply_delta(self.exit_miss_delta);
                    let lmt = order.limit;
                    
                    // submit order and record the request
                    println!("replacing");
                    let rx = broker::replace(&mut self.broker_ch, *id, order)
                        .await
                        .expect("broker request channel closed");
                    self.pending_request = PendingRequest::Replace {
                        rsp: rx,
                        qty: os.qty,
                        lmt: lmt,
                    };
                }
            }
        }
    }

    // Try to yield a response to any pending requests.
    fn _try_yield(&mut self, now_ts: i64) {
        use oneshot::error::TryRecvError;
        match &mut self.pending_request {
            PendingRequest::Submit { rsp, qty, lmt } => {
                match rsp.try_recv() {
                    Ok(res) => {
                        // got response
                        match res {
                            Ok(id) => {
                                // order submit succeeded
                                let state = OrderState {
                                    qty: *qty,
                                    limit: *lmt,
                                    filled_qty: Decimal::from(0),
                                    filled_price: None,
                                    timestamp: now_ts,
                                };
                                self.active_order = Some((id, state));
                                self.failed_requests = 0;
                            },
                            Err(e) => {
                                // order submit failed
                                self._handle_broker_err(e);
                            }
                        }

                        // request no longer pending
                        self.pending_request = PendingRequest::None;
                    },
                    Err(TryRecvError::Empty) => (), // not ready
                    Err(TryRecvError::Closed) => {
                        panic!("broker response channel closed without yielding result");
                    },
                }
            },
            PendingRequest::Replace { rsp, qty, lmt } => {
                match rsp.try_recv() {
                    Ok(res) => {
                        // got response
                        match res {
                            Ok(id) => {
                                // order replace succeeded
                                let state = OrderState {
                                    qty: *qty,
                                    limit: *lmt,
                                    filled_qty: Decimal::from(0),
                                    filled_price: None,
                                    timestamp: now_ts,
                                };
                                println!("replace id: {}", id);
                                self.replace_order = Some((id, state));
                                self.failed_requests = 0;
                            },
                            Err(e) => {
                                // order replace failed
                                self._handle_broker_err(e);
                            }
                        }

                        // request no longer pending
                        self.pending_request = PendingRequest::None;
                    },
                    Err(TryRecvError::Empty) => (), // not ready
                    Err(TryRecvError::Closed) => {
                        panic!("broker response channel closed without yielding result");
                    },
                }
            },
            PendingRequest::Cancel(rsp) => {
                match rsp.try_recv() {
                    Ok(res) => {
                        // got response
                        match res {
                            Ok(_) => {
                                // order cancel succeeded
                                // NOTE: don't record order cancel here, instead defer until
                                // an order status update with finished == true
                                self.failed_requests = 0;
                            },
                            Err(e) => {
                                // order cancel failed
                                eprintln!("cancel failed");
                                self._handle_broker_err(e);
                            }
                        }

                        // request no longer pending
                        self.pending_request = PendingRequest::None;
                    },
                    Err(TryRecvError::Empty) => (), // not ready
                    Err(TryRecvError::Closed) => {
                        panic!("broker response channel closed without yielding result");
                    },
                }
            },
            PendingRequest::None => (),
        }
    }

    fn _handle_broker_err(&mut self, e: broker::Error) {
        match e {
            broker::Error::Internal | broker::Error::Rejection => {
                self.failed_requests += 1;
            },
            _ => {
                panic!("unexpected error: {:?}", e)
            },
        }

        if self.failed_requests > 5 {
            panic!("more than 5 consecutive broker requests failed");
        }
    }
}

/// Run the strategy.
pub async fn run(
    config: Config,
    endpoint: Endpoint,
    mkt_open: DateTime<Utc>,
    mkt_close: DateTime<Utc>,
) {
    if !config.is_valid() {
        return;
    }

    // break up endpoint
    let mut mktdata_ch = endpoint.mktdata_rx;
    let broker_ch = endpoint.broker_request_tx;
    let mut update_ch = endpoint.order_update_rx;

    // price data
    let mut average_price: Option<Decimal> = None;
    let mut last_average_price: Option<Decimal> = None;
    let mut volume = Decimal::from(0);
    let mut notional = Decimal::from(0);

    // latency data
    let mut average_latency: Option<i64> = None;
    let mut trades_received = 0i64;
    let mut latency_target = 0i64;

    // alpha values for EMAs
    let smooth = Decimal::from(2);
    let short_alpha = smooth / (config.short_span_secs + Decimal::from(1));
    let long_alpha = smooth / (config.long_span_secs + Decimal::from(1));
    // NOTE: unwrap because alpha should be acceptable if configuration is valid
    let mut short_ema = EMA::new(short_alpha).unwrap();
    let mut long_ema = EMA::new(long_alpha).unwrap();
    let mut was_long = false;

    let mut executor = Executor {
        asset: config.asset,
        bid: None,
        ask: None,
        target_shares: Decimal::from(0),
        last_shares: Decimal::from(0),
        target_price: None,
        exiting: false,
        exit_limit_missed: false,
        entry_delta: config.entry_delta,
        exit_delta: config.exit_delta,
        exit_miss_delta: config.exit_miss_delta,
        active_order: None,
        replace_order: None,
        pending_request: PendingRequest::None,
        failed_requests: 0,
        broker_ch: broker_ch,
    };

    // main event loop
    let mut target_ts: i64 = 0; // target time
    loop {
        let now = chrono::Utc::now();

        // check if the current time is greater than the target time
        let now_ts = now.timestamp_millis();
        if now_ts >= target_ts {
            // forward fill average price
            if average_price == None {
                average_price = last_average_price;
            }

            // Only update EMAs if an average price is available. Note that due to the forward
            // fill above, this condition only occurs at the very start of data collection.
            // If no trades occur for any T in t...t+n, where t is the start of data collection,
            // EMA(T) will not exist.
            if let Some(a) = average_price {
                let a = a.round_dp_with_strategy(2, RoundingStrategy::RoundHalfUp);

                short_ema.update(a);
                long_ema.update(a);

                // only compare the EMAs for T in START...(END-10 seconds)
                if now >= mkt_open && now < mkt_close - chrono::Duration::seconds(10) {
                    if short_ema.value() <= long_ema.value() && !was_long {
                        executor.target_shares = config.shares;
                        executor.target_price = average_price;
                        was_long = true;
                    } else if short_ema.value() >= long_ema.value() && was_long {
                        executor.target_shares = -config.shares;
                        executor.target_price = average_price;
                        was_long = false;
                    }
                }
            }

            // adjust target to the start of the next second plus a cushion based on latency
            latency_target = average_latency.unwrap_or(latency_target).abs();
            let plus_one = now_ts + 1000;
            target_ts = plus_one - (plus_one % 1000) + (latency_target / 2);

            // reset trade data
            last_average_price = average_price;
            average_price = None;
            volume = Decimal::from(0);
            notional = Decimal::from(0);

            // reset latency data
            average_latency = None;
            trades_received = 0;
        }

        // now check for strategy end conditions
        if now >= mkt_close - chrono::Duration::seconds(10) {
            executor.target_shares = Decimal::from(0);
            executor.target_price = average_price;

            if now >= mkt_close {
                return;
            }
        }

        executor.resolve(now_ts).await;

        // wait for new market data, order update, or timeout
        let new_mktdata = mktdata_ch.recv().fuse();
        let order_update = update_ch.recv().fuse();
        let timeout = delay_for(Duration::from_millis((target_ts - now_ts) as u64)).fuse();
        pin_mut!(new_mktdata, order_update, timeout);
        select! {
            mktdata = new_mktdata => {
                match mktdata {
                    Ok(MktData::Trade(trade)) => {
                        if trade.asset == config.asset {
                            // compute average price
                            let trade_notional = trade.size * trade.price;
                            volume = volume + trade.size;
                            notional = notional + trade_notional;
                            average_price = Some(notional / volume);

                            // compute average latency
                            let time_processed = chrono::Utc::now().timestamp_millis();
                            let mut total_latency = average_latency.unwrap_or(0) * trades_received;
                            total_latency += time_processed - trade.timestamp;
                            trades_received += 1;
                            average_latency = Some(total_latency / trades_received);
                        }
                    },
                    Ok(MktData::Quote(quote)) => {
                        if quote.asset == config.asset {
                            executor.update_ba(quote.bid, quote.ask);
                        }
                    },
                    _ => (),
                }
            },
            update = order_update => {
                if let Ok(status) = update {
                    executor.update_order_status(status);
                }
            }
            _ = timeout => ()
        }
    }
}
