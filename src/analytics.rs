//! Numerical analysis functionality useful for implementing algorithms and models.

#![allow(unused)]

use crate::types::Decimal;
use std::fmt;

/// An Exponential Moving Average object, used to calculate EMA values.
#[derive(Debug, Clone, Copy)]
pub struct EMA {
    alpha: Decimal,
    value: Option<Decimal>,
}

impl EMA {
    /// Create a new EMA with a given decay factor alpha.
    pub fn new(alpha: Decimal) -> Result<EMA, &'static str> {
        if alpha > 0.into() && alpha <= 1.into() {
            Ok(EMA {
                alpha: alpha,
                value: None,
            })
        } else {
            Err("violated constraint 0 < alpha <= 1")
        }
    }

    /// Get the value of an EMA.
    pub fn value(&self) -> Option<Decimal> {
        self.value
    }

    /// Calculate an EMA's current value given an input value.
    pub fn update(&mut self, input: Decimal) -> &mut EMA {
        if let Some(last) = self.value {
            let res = self.alpha * input + (Decimal::from(1) - self.alpha) * last;
            self.value = Some(res);
        } else {
            self.value = Some(input);
        }
        self
    }

    /// Calculate an EMA's current value given an iterator of input values.
    pub fn with<I>(&mut self, input: I) -> &mut EMA
    where
        I: IntoIterator<Item = Decimal>,
    {
        let mut values = input.into_iter();
        if let Some(initial) = values.nth(0) {
            let res = values.fold(initial, |l, c| {
                self.alpha * c + (Decimal::from(1) - self.alpha) * l
            });
            self.value = Some(res);
        }
        self
    }
}

impl fmt::Display for EMA {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.value {
            Some(v) => write!(f, "{}", v),
            None => write!(f, "null"),
        }
    }
}
