all: release

clean:
	cargo clean

debug:
	cargo install

install-etc:
	mkdir -p $(HOME)/.local/bin
	mkdir -p $(HOME)/.config/systemd/user
	mkdir -p $(HOME)/.tachyon
	cp tlaunch $(HOME)/.local/bin/
	cp systemd/tachyon.service $(HOME)/.config/systemd/user/
	cp systemd/tachyon.timer $(HOME)/.config/systemd/user/
	cp default.config $(HOME)/.tachyon/config

install-debug: install-etc debug
	cp target/debug/tachyon $(HOME)/.local/bin/

install: install-etc release
	cp target/release/tachyon $(HOME)/.local/bin/

release:
	cargo build --release